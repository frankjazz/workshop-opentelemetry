<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<meta name="description" content="OpenTelemetry Workshop">
		<meta name="author" content="Paige Cruz">

		<title>Lab 3 - Programmatic Instrumentation</title>

		<link rel="stylesheet" href="dist/reset.css">
		<link rel="stylesheet" href="dist/reveal.css">
		<link rel="stylesheet" href="dist/theme/chrono.css">

		<!-- Theme used for syntax highlighted code -->
		<link rel="stylesheet" href="plugin/highlight/monokai.css">
	</head>
	<body>
		<div class="reveal">
			<div class="slides">
				<section>
					<h3 class="r-fit-text">Lab 3 - Programmatic Instrumentation</h3>
				</section>
				<section>
					<div style="width: 1056px; height: 250px;">
						<h2>Lab Goal</h2>
						<h4>
							This lab walks you through programmatically instrumenting the demo application with
							OpenTelemetry libraries, and viewing trace data in Jaeger.
						</h4>
					</div>
				</section>

                <section>
					<div style="width: 1056px; height: 120px;">
						<h2 class="r-fit-text">Programmatic instrumentation</h2>
					</div>
					<div style="width: 1056px; height: 250px; text-align: left; font-size: xx-large;">
						Using language-specific or framework-specific instrumentation libraries. Example is the demo
						application is Flask app written in Python - we will use the
						<code>opentelemetry-instrumentation-flask</code> library to <em>programmatically</em> instrument
						web requests.
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 120px;">
						<h2 class="r-fit-text">Resetting your app.py file</h2>
					</div>
					<div style="width: 1056px; height: 50px; text-align: left; font-size: xx-large;">
						Open <strong>app.py</strong> and remove previous instrumentation so the file looks like:
					</div>
					<div style="width: 900px; height: 250px; font-size: large;">
						<pre>
							<code data-trim data-noescape>
								import random
								import os
								import requests
								from flask import Flask, render_template

								app = Flask(__name__)

								HITS = 0

								@app.route('/')
								def index():
									global HITS
									HITS = HITS + 1
									msg = f'This webpage has been viewed {HITS} times'
									return msg

								@app.route("/rolldice")
								def roll_dice():
									result = do_roll()
									return result

								def do_roll():
									r = str(random.randint(1, 6))
									return r

								@app.route('/doggo')
								def fetch_dog():
									resp = requests.get("https://dog.ceo/api/breeds/image/random")
									if resp.status_code > 300:
										return "Error!"
									else: 
										image_src = resp.json()["message"]
										return render_template('random-pet-pic.html', img_src=image_src)
									return 
									

								if __name__ == "__main__":
									app.run(host="0.0.0.0")
							</code>
						</pre>
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 120px;">
						<h2 class="r-fit-text">Importing the OpenTelemetry libraries</h2>
					</div>
					<div style="width: 1056px; height: 70px; text-align: left; font-size: x-large;">
						Update imports to include these libraries. We're using <code>opentelemetry-instrumentation-flask</code>
						to programmatically instrument Flask web requests and
						<a href="https://github.com/open-telemetry/opentelemetry-python" target="_blank">Python OTel SDK library</a>:
					</div>
					<div style="width: 900px; height: 250px; font-size: xx-large;">
						<pre>
							<code data-trim data-noescape>
								import random
								import os
								import requests
								from flask import Flask, render_template
								from opentelemetry.instrumentation.flask import FlaskInstrumentor
								from opentelemetry.sdk.trace import TracerProvider
								from opentelemetry.sdk.trace.export import (
									BatchSpanProcessor,
									ConsoleSpanExporter,
								)
								from opentelemetry.trace import get_tracer_provider, set_tracer_provider

								... 
							</code>
						</pre>
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 120px;">
						<h2 class="r-fit-text">Configure the OpenTelemetry SDK</h2>
					</div>
					<div style="width: 1056px; height: 70px; text-align: left; font-size: xx-large;">
						Set up the SDK for tracing <em>after</em> the import statements and above any existing code:
					</div>
					<div style="width: 900px; height: 250px; font-size: xx-large;">
						<pre>
							<code data-trim data-noescape>
								...
								from opentelemetry.trace import get_tracer_provider, set_tracer_provider

								set_tracer_provider(TracerProvider())
								get_tracer_provider().add_span_processor(
									BatchSpanProcessor(ConsoleSpanExporter())
								)

								app = Flask(__name__)

								... 
							</code>
						</pre>
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 100px;">
						<h2 class="r-fit-text">Configure programmatic Flask instrumentation</h2>
					</div>
					<div style="width: 1056px; height: 70px; text-align: left; font-size: xx-large;">
						Set up the programmatic instrumentation <em>before</em> the Flask module is created and then
						pass the module to the instrumentor:
					</div>
					<div style="width: 900px; height: 250px; font-size: xx-large;">
						<pre>
							<code data-trim data-noescape>
								...
								from opentelemetry.trace import get_tracer_provider, set_tracer_provider

								set_tracer_provider(TracerProvider())
								get_tracer_provider().add_span_processor(
									BatchSpanProcessor(ConsoleSpanExporter())
								)

								instrumentor = FlaskInstrumentor()
								app = Flask(__name__)
								instrumentor.instrument_app(app)
								... 
							</code>
						</pre>
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 100px;">
						<h2 class="r-fit-text">Confirm new instrumentation</h2>
					</div>
					<div style="width: 1056px; height: 50px; text-align: left; font-size: xx-large;">
						Your <em>app.py</em> file should now be set up like this (scroll to view entire file):
					</div>
					<div style="width: 900px; height: 300px; font-size: large;">
						<pre>
							<code data-trim data-noescape>
								import random
								import os
								import requests
								from flask import Flask, render_template
								from opentelemetry.instrumentation.flask import FlaskInstrumentor
								from opentelemetry.sdk.trace import TracerProvider
								from opentelemetry.sdk.trace.export import (
									BatchSpanProcessor,
									ConsoleSpanExporter,
								)
								from opentelemetry.trace import get_tracer_provider, set_tracer_provider

								set_tracer_provider(TracerProvider())
								get_tracer_provider().add_span_processor(
									BatchSpanProcessor(ConsoleSpanExporter())
								)

								instrumentor = FlaskInstrumentor()
								app = Flask(__name__)
								instrumentor.instrument_app(app)

								HITS = 0

								@app.route('/')
								def index():
									global HITS
									HITS = HITS + 1
									msg = f'This webpage has been viewed {HITS} times'
									return msg

								@app.route("/rolldice")
								def roll_dice():
									result = do_roll()
									return result

								def do_roll():
									r = str(random.randint(1, 6))
									return r

								@app.route('/doggo')
								def fetch_dog():
									resp = requests.get("https://dog.ceo/api/breeds/image/random")
									if resp.status_code > 300:
										return "Error!"
									else: 
										image_src = resp.json()["message"]
										return render_template('random-pet-pic.html', img_src=image_src)
									return 
									

								if __name__ == "__main__":
									app.run(host="0.0.0.0")
							</code>
						</pre>
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 120px;">
						<h2 class="r-fit-text">Rebuilding the container image</h2>
					</div>
					<div style="width: 1056px; height: 50px; text-align: left; font-size: xx-large;">
						Build an image with new programmatic instrumentation:
					</div>
					<div style="width: 900px; height: 70px;">
						<pre>
							<code data-trim data-noescape>
								$ podman build -t hello-otel:programmatic -f Dockerfile
							</code>
						</pre>
					</div>
					<div style="width: 1056px; height: 50px; text-align: left; font-size: xx-large;">
						Verify you get a success message in the console like below:
					</div>
					<div style="width: 1000px; height: 100px;">
						<pre>
							<code data-trim data-noescape>
								Successfully tagged localhost/hello-otel:programmatic   \
								495118b9c78178356fc0cbd05d244e387f3fdc379b1b5c873e76b1cb41b82ef5
							</code>
						</pre>
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 120px;">
						<h2 class="r-fit-text">Run the rebuilt container</h2>
					</div>
					<div style="width: 1056px; height: 50px; text-align: left; font-size: xx-large;">
						Enter this command in the console to run the container:
					</div>
					<div style="width: 1000px; height: 200px; font-size: xx-large;">
						<pre>
							<code data-trim data-noescape>
								podman run -i -p 8001:8000 -e FLASK_RUN_PORT=8000 hello-otel:programmatic opentelemetry-instrument \
								  --traces_exporter console \
								  --metrics_exporter console \
								  --service_name hello-otel \
								  flask run --host=0.0.0.0
							</code>
						</pre>
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 120px;">
						<h2 class="r-fit-text">Verifying the rebuilt container</h2>
					</div>
					<div style="width: 1056px; height: 70px; text-align: left; font-size: xx-large;">
						Open <a href="http://localhost:8001/" target="_blank"> http://localhost:8001</a> in your
						browser and confirm you see this displayed:
					</div>
					<div style="width: 900px; height: 150px;">
						<pre>
							<code data-trim data-noescape>
								This webpage has been viewed 1 times
							</code>
						</pre>
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 100px;">
						<h2 class="r-fit-text">Verify programmatic instrumentation</h2>
					</div>
					<div style="width: 1056px; height: 70px; text-align: left; font-size: xx-large;">
						Open <a href="http://localhost:8001/rolldice" target="_blank">http://localhost:8001/rolldice</a>
						in your browser and confirm you see a span in the console like:
					</div>
					<div style="width: 900px; height: 200px; font-size: large;">
						<pre>
							<code data-trim data-noescape>
								{
									"name": "/rolldice",
									"context": {
										"trace_id": "0x10bda779f22edf7a7bf278cee1c02ad6",
										"span_id": "0x13f72a638845d90b",
										"trace_state": "[]"
									},
									"kind": "SpanKind.SERVER",
									"parent_id": null,
									"start_time": "2023-06-22T00:31:04.019207Z",
									"end_time": "2023-06-22T00:31:04.019987Z",
									"status": {
										"status_code": "UNSET"
									},
									"attributes": {
										"http.method": "GET",
										"http.server_name": "0.0.0.0",
										"http.scheme": "http",
										"net.host.port": 8000,
										"http.host": "localhost:8001",
										"http.target": "/rolldice",
										"net.peer.ip": "10.88.0.14",
										"http.user_agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (HTML, like Gecko) Chrome/114.0.0.0 Safari/537.36",
										"net.peer.port": 49606,
										"http.flavor": "1.1",
										"http.route": "/rolldice",
										"http.status_code": 200
									},
									"events": [],
									"links": [],
									"resource": {
										"attributes": {
											"telemetry.sdk.language": "python",
											"telemetry.sdk.name": "opentelemetry",
											"telemetry.sdk.version": "1.18.0",
											"service.name": "unknown_service"
										},
										"schema_url": ""
									}
								}
							</code>
						</pre>
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 150px;">
						<h2>Stop the container</h2>
					</div>
					<div style="width: 1056px; height: 120px; text-align: left; font-size: xxx-large;">
						<p>Enter CTRL-C in the console to stop the container</p>
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 120px;">
						<h2 class="r-fit-text">Setting up Jaeger tooling</h2>
					</div>
					<div style="width: 1056px; height: 70px; text-align: left; font-size: xx-large;">
						Open <strong>./app_pod.yaml</strong> and review the section with the Jaeger all-in-one image:
					</div>
					<div style="width: 900px; height: 250px; font-size: xx-large;">
						<pre>
							<code data-trim data-noescape>
								- name: jaeger-all-in-one
								image: jaegertracing/all-in-one:1.42
								resources:
								  limits:
									memory: "128Mi"
									cpu: "500m"
								ports:
								- containerPort: 16686
								  hostPort: 16686
								- containerPort: 4318
								env:
								- name: COLLECTOR_OTLP_ENABLED
								  value: "true"
								- name: OTEL_TRACES_EXPORTER
								  value: "otlp"
							</code>
						</pre>
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 120px;">
						<h2 class="r-fit-text">Running containers in a pod</h2>
					</div>
					<div style="width: 1056px; height: 50px; text-align: left; font-size: xx-large;">
						Run the sample application and Jaeger containers in a Pod.
					</div>
					<div style="width: 900px; height: 70px;">
						<pre>
							<code data-trim data-noescape>
								$ podman play kube app_pod.yaml
							</code>
						</pre>
					</div>
					<div style="width: 1056px; height: 50px; text-align: left; font-size: xx-large;">
						Verify you get a success message in the console like below:
					</div>
					<div style="width: 900px; height: 200px; font-size: xx-large;">
						<pre>
							<code data-trim data-noescape>
								Pod:
								0bc7dbf0bf9802c3324630d42a2109b93055d0f6b3c0ee2c83d55f954e56643a

								Containers:
								bd176546950d48ea01d6bde2fa08f5bea81fb62e279856016b90053016409499
								5b9a521a8408d549fed9fff85b07333a6b5e772224d6bda257d834f416966729
							</code>
						</pre>
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 120px;">
						<h2 class="r-fit-text">Verifying the Jaeger setup</h2>
					</div>
					<div style="width: 1056px; height: 70px; text-align: left; font-size: xx-large;">
						Open browser to <a href="http://localhost:16686" target="_blank">http://localhost:16686</a>.
						You should see a screen like below with a Gopher detective:
					</div>
					<div style="width: 1056px; height: 450px;">
						<img src="images/lab02-jaeger-successful.png" alt="gopher">
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 120px;">
						<h2 class="r-fit-text">Visiting the app endpoints</h2>
					</div>
					<div style="width: 1056px; height: 350px; text-align: left; font-size: xx-large;">
						Open a browser and make several requests to the different application endpoints:<br />
						<br />
						<ul>
							<li><a href="http://localhost:8001" target="_blank">http://localhost:8001</a></li>
							<li><a href="http://localhost:8001/doggo" target="_blank">http://localhost:8001/doggo</a></li>
							<li><a href="http://localhost:8001/rolldice" target="_blank">http://localhost:8001/rolldice</a></li>
						</ul>
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 120px;">
						<h2 class="r-fit-text">Viewing traces in Jaeger</h2>
					</div>
					<div style="width: 1056px; height: 120px; text-align: left; font-size: xx-large;">
						Open the Jaeger UI at <code>http://localhost:16686</code> and select <strong>hello-otel</strong>
						from the service dropdown:

						Confirm that you see traces returned for the endpoints you requested.
					</div>
					<div style="width: 1056px; height: 370px; text-align: center;">
						<img src="images/lab03-jaeger.png" alt="hello-otel">
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 120px;">
						<h2 class="r-fit-text">Viewing a trace waterfall</h2>
					</div>
					<div style="width: 1056px; height: 70px; text-align: left; font-size: xx-large;">
						Select one of the traces by clicking the span name and confirm you see a trace waterfall like
						this:
					</div>
					<div style="width: 1056px; height: 450px; text-align: center;">
						<img src="images/lab03-jaeger-waterfall.png" alt="traces">
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 120px;">
						<h2 class="r-fit-text">Lab completed - Results</h2>
					</div>
					<div style="width: 1056px; height: 70px; font-size: xx-large;">
						We installed and configured OpenTelemetry SDK programmatically in the demo application and successfully
					    sent and viewed traces in Jaeger.
					</div>
					<div style="width: 1056px; height: 350px; text-align: center;">
						<img src="images/lab03-jaeger-waterfall.png" alt="traces">
					</div>
					<div style="width: 1056px; height: 70px; font-size: xx-large;">
						<br />
						Leave your pod running because next up, is exploring trace data in Jaeger!
					</div>
				</section>

				<section data-background="images/questions.png">
					<span class="menu-title" style="display: none">References</span>
					<div style="width: 1056px; height: 200px;">
						<img height="150" width="100%" src="images/references.jpg" alt="references">
					</div>
					<div style="width: 1056px; height: 400px; font-size: xx-large; text-align: left">
						<ul>
							<li><a href="https://o11y-workshops.gitlab.io/" target="_blank">Getting started with cloud native o11y workshops</a></li>
							<li><a href="https://github.com/perses/perses" target="_blank">Perses project repository</a></li>
							<li><a href="https://gitlab.com/o11y-workshops/workshop-perses" target="_blank">This workshop project repository</a></li>
							<li><a href="https://gitlab.com/o11y-workshops" target="_blank">O11y workshop collection</a></li>
						</ul>
					</div>
				</section>

				<section>
					<span class="menu-title" style="display: none">Questions or feedback?</span>
					<div style="width: 1056px; height: 150px">
						<h2 class="r-fit-text">Contact - are there any questions?</h2>
					</div>
					<div style="width: 1056px; height: 200px; font-size: x-large; text-align: left">
						Paige Cruz<br/>
						Senior Developer Advocate<br/>
						Contact: <a href="https://twitter.com/paigerduty" target="_blank">Twitter</a>
						<a href="https://hachyderm.io/@epaigerduty" target="_blank">Mastodon</a>
						or <a href="mailto:paigerduty@chronosphere.io">Send Email</a>
					</div>
				</section>

				<section>
					<div style="width: 1056px; height: 250px;">
						<h2 class="r-fit-text">Up next in workshop...</h2>
					</div>
					<div style="width: 1056px; height: 200px; font-size: xxx-large;">
						<a href="lab04.html" target="_blank">Lab 04 - Exploring Traces with Jaeger</a>
					</div>
				</section>
			</div>
		</div>

		<script src="dist/reveal.js"></script> 
		<script src="plugin/notes/notes.js"></script>
		<script src="plugin/markdown/markdown.js"></script>
		<script src="plugin/highlight/highlight.js"></script>
		<script>
			// More info about initialization & config:
			// - https://revealjs.com/initialization/
			// - https://revealjs.com/config/
			Reveal.initialize({
				hash: true,

				// Learn about plugins: https://revealjs.com/plugins/
				plugins: [ RevealMarkdown, RevealHighlight, RevealNotes ]
			});
		</script>
		<script src="node_modules/reveal.js-menu/menu.js"></script>
		<script>
			Reveal.initialize({
				plugins: [ RevealMenu ]
			});
		</script>
	</body>
</html>